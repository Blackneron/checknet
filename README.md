# CheckNet - README #
---

### Overview ###

The **CheckNet** tool checks periodically the connection to the Internet. The tool tries to connect to 2 reference servers. A small colored (red or green) rectangle shows the actual connection status. The tool also creates an audio alarm if the connection is lost.

### Screenshots ###

![CheckNet - Start Message](development/readme/checknet1.png "CheckNet - Start Message")

![CheckNet - Error Message](development/readme/checknet2.png "CheckNet - Error Message")

![CheckNet - Net Status OK](development/readme/checknet3.png "CheckNet - Net Status OK")

![CheckNet - Net Status Checking](development/readme/checknet4.png "CheckNet - Net Status Checking")

![CheckNet - Net Status Error](development/readme/checknet5.png "CheckNet - Net Status Error")

![CheckNet - Show Settings](development/readme/checknet6.png "CheckNet - Show Settings")

![CheckNet - Show Hotkeys](development/readme/checknet7.png "CheckNet - Show Hotkeys")

![CheckNet - Version Info](development/readme/checknet8.png "CheckNet - Version Info")

### Setup ###

* Copy the directory **checknet** to your PC.
* Start the **CheckNet.exe** executable with a doubleclick.
* Press **CTRL + ALT + H** to display the hotkey/shortcut window.
* Press **CTRL + ALT + O** to open and edit the configuration file.
* If you made changes, press **CTRL + ALT + R** to restart the tool.
* With the restart the configuration changes will be applied.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **CheckNet** tool is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
