; ======================================================================
;
;                  CheckNet - Check Internet Connection
;
;                              Version 1.0
;
;               Copyright 2015 / PB-Soft / Patrick Biegel
;
;                            www.pb-soft.com
;
; ======================================================================

; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   S E T T I N G S
; ======================================================================

; Enable autotrim.
AutoTrim, On

; Specify that only one instance of this application can run.
#SingleInstance ignore

; Set the script speed.
SetBatchLines, 20ms

; Set the working directory.
SetWorkingDir, %A_ScriptDir%


; ======================================================================
; S P E C I F Y   T H E   A P P L I C A T I O N   I N F O R M A T I O N
; ======================================================================

; Specify the application name.
ApplName = CheckNet

; Specify the application version.
ApplVersion = 1.0

; Specify the internal application version.
ApplIntVersion = 64

; Calculate the application modification time.
FileGetTime, ModificationTime, CheckNet.exe

; Format the application build date.
FormatTime, ApplBuildDate , %ModificationTime%, dd.MM.yyyy / HH:mm:ss

; Specify the application website.
ApplWebsite = http://pb-soft.com

; Specify the copyright text.
Copyright := "Copyright " . Chr(169) . " " . A_YYYY . " - PB-Soft"


; ======================================================================
; I N I T I A L I Z E   V A R I A B L E S
; ======================================================================

; Specify the minimum sleep time in seconds.
MinSleepTime = 10

; Specify the name of the configuration file.
ConfigurationFile = %A_ScriptDir%\config\checknet.ini

; Specify the location of the temporary file.
TempFile = %A_ScriptDir%\%ApplName%.tmp


; ======================================================================
; C R E A T E   D I R E C T O R I E S
; ======================================================================

; Check if the configuration directory does not exist.
IfNotExist, %A_ScriptDir%\config
{

  ; Create the configuration directory.
  FileCreateDir, %A_ScriptDir%\config

} ; Check if the configuration directory does not exist.

; Check if the license directory does not exist.
IfNotExist, %A_ScriptDir%\license
{

  ; Create the license directory.
  FileCreateDir, %A_ScriptDir%\license

} ; Check if the license directory does not exist.

; Check if the sound directory does not exist.
IfNotExist, %A_ScriptDir%\sound
{

  ; Create the sound directory.
  FileCreateDir, %A_ScriptDir%\sound

} ; Check if the sound directory does not exist.


; ======================================================================
; F I L E   I N S T A L L A T I O N
; ======================================================================

; Install the configuration file.
FileInstall, config\checknet.ini, config\checknet.ini, 0

; Install the sound files.
FileInstall, sound\alarm.wav, sound\alarm.wav, 0
FileInstall, sound\check.wav, sound\check.wav, 0
FileInstall, sound\ok.wav, sound\ok.wav, 0

; Install the license files.
FileInstall, license\freesound.png, license\freesound.png, 0
FileInstall, license\alarm.png, license\alarm.png, 0
FileInstall, license\check.png, license\check.png, 0
FileInstall, license\cc0.png, license\cc0.png, 0
FileInstall, license\ok.png, license\ok.png, 0


; ======================================================================
; R E A D   C O N F I G U R A T I O N
; ======================================================================

; Read the audio setting.
IniRead, AudioOn, %ConfigurationFile%, Configuration, AudioOn, 1

; Read the ballon setting.
IniRead, BalloonOn, %ConfigurationFile%, Configuration, BalloonOn, 1

; Read the sleep time setting.
IniRead, SleepTime, %ConfigurationFile%, Configuration, SleepTime, 120

; Read the url of the reference server 1.
IniRead, ReferenceServer1, %ConfigurationFile%, Configuration, ReferenceServer1

; Read the url of the reference server 2.
IniRead, ReferenceServer2, %ConfigurationFile%, Configuration, ReferenceServer2

; Read the window X position.
IniRead, WindowPositionX, %ConfigurationFile%, Configuration, WindowPositionX, Center

; Read the window Y position.
IniRead, WindowPositionY, %ConfigurationFile%, Configuration, WindowPositionY, 0

; Read the window X size.
IniRead, WindowSizeX, %ConfigurationFile%, Configuration, WindowSizeX, 80

; Read the window Y size.
IniRead, WindowSizeY, %ConfigurationFile%, Configuration, WindowSizeY, 40

; Read if the menu window should be on top of all other windows.
IniRead, OnTop, %ConfigurationFile%, Configuration, OnTop, 1


; ======================================================================
; S P E C I F Y   T H E   S L E E P T I M E
; ======================================================================

; Check if the sleep time is lower than the minimum sleep time.
If (SleepTime < MinSleepTime) {

  ; Display a warning message.
  MsgBox, 4144, %ApplName% %ApplVersion% - Warning !, The configured sleep time is %SleepTime% seconds !`nThe minimum sleep time is %MinSleepTime% seconds !`nSetting the sleep time to %MinSleepTime% seconds !

	; Specify the minimum sleep time in seconds.
	SleepTime := MinSleepTime

} ; Check if the sleep time is lower than the minimum sleep time.

; Calculate the sleep time in miliseconds.
SleepTime := SleepTime * 1000


; ======================================================================
; S T A R T   M E S S A G E
; ======================================================================

; Check if the balloon messages should be displayed.
if (BalloonOn = 1) {

  ; Display an information balloon message.
  TrayTip, %ApplName% %ApplVersion%, The %ApplName% application was successfully started !, 10, 1

} ; Check if the balloon messages should be displayed.


; ======================================================================
; C H E C K   W I N D O W   P O S I T I O N   -   H O R I Z O N T A L
; ======================================================================

; Check if the horizontal window position is not specified.
If (WindowPositionX = "")
{

  ; Specify the horizontal window position.
  GUIPosX = Center

  ; Save the horizontal window position.
  IniWrite, %A_Space%Center, %ConfigurationFile%, Configuration, WindowPositionX
}

; Check if the horizontal window position is centered.
If (WindowPositionX = "Center")
{

  ; Specify the horizontal window position.
  GUIPosX := WindowPositionX
}

; The horizontal window position is not centered.
else
{

  ; Check if the horizontal window position is valid.
  If (WindowPositionX >= 0 && WindowPositionX <= (A_ScreenWidth - WindowSizeX))
  {

    ; Specify the horizontal window position.
    GUIPosX := WindowPositionX
  }

  ; The horizontal window position is not valid.
  else
  {

    ; Specify the horizontal window position.
    GUIPosX = Center
  }

} ; The horizontal window position is not centered.


; ======================================================================
; C H E C K   W I N D O W   P O S I T I O N   -   V E R T I C A L
; ======================================================================

; Check if the vertical window position is not specified.
If (WindowPositionY = "")
{

  ; Specify the vertical window position.
  GUIPosY = Center

  ; Save the vertical window position.
  IniWrite, %A_Space%Center, %ConfigurationFile%, Configuration, WindowPositionY
}

; Check if the vertical window position is centered.
If (WindowPositionY = "Center")
{

  ; Specify the vertical window position.
  GUIPosY := WindowPositionY
}

; The vertical window position is not centered.
else
{

  ; Check if the vertical window position is valid.
  If (WindowPositionY >= 0 && WindowPositionY <= (A_ScreenHeight - WindowSizeY))
  {

    ; Specify the vertical window position.
    GUIPosY := WindowPositionY
  }

  ; The vertical window position is not valid.
  else
  {

    ; Specify the vertical window position.
    GUIPosY = Center
  }

} ; The vertical window position is not centered.


; ======================================================================
; C H E C K   W I N D O W   S I Z E   -   H O R I Z O N T A L
; ======================================================================

; Check if the horizontal window size is not specified.
If (WindowSizeX = "" || WindowSizeX < 20 || WindowSizeX > A_ScreenWidth)
{

  ; Specify the horizontal window size.
  WindowSizeX = 80

  ; Save the horizontal window size.
  IniWrite, %A_Space%%WindowSizeX%, %ConfigurationFile%, Configuration, WindowSizeX

} ; Check if the horizontal window size is not specified.


; ======================================================================
; C H E C K   W I N D O W   S I Z E   -   V E R T I C A L
; ======================================================================

; Check if the vertical window size is not specified.
If (WindowSizeY = "" || WindowSizeY < 20 || WindowSizeY > A_ScreenHeight)
{

  ; Specify the vertical window size.
  WindowSizeY = 40

  ; Save the vertical window size.
  IniWrite, %A_Space%%WindowSizeY%, %ConfigurationFile%, Configuration, WindowSizeY

} ; Check if the vertical window size is not specified.


; ======================================================================
; D I S P L A Y   T H E   G U I
; ======================================================================

; Check if the window should be on top of all other windows.
if OnTop = 1
{

  ; Set the window to top of others.
  GUI +AlwaysOnTop

} ; Check if the window should be on top of all other windows.

; Specify the GUI options.
Gui, +ToolWindow -SysMenu -Caption

; Specify the background color of the GUI.
Gui, Color, 1DB400

; Specify the GUI font.
Gui, font, s8 q5 bold, MS sans serif
Gui, font, s8 q5 bold, Verdana
Gui, font, s8 q5 bold, Arial

; Display the GUI.
Gui, Show, x%GUIPosX% y%GUIPosY% w%WindowSizeX% h%WindowSizeY%, %ApplName%


; ======================================================================
; C H E C K   I N T E R N E T   C O N N E C T I O N
; ======================================================================

; Initialize the caption flag.
CaptionFlag = 0

; Initialize the error flag.
ErrorFlag = 0

; Initialize the GUI flag.
GUIFlag = 1

; Loop to check the internet connection
Loop {

  ; Specify the background color of the GUI.
  Gui, Color, FFB519

  ; Check if the audio is enabled.
  if (AudioOn = 1) {

    ; Play the check signal.
    SoundPlay, %A_ScriptDir%\sound\check.wav

  } ; Check if the audio is enabled.

  ; Try to download a html page from the first reference server.
  UrlDownloadToFile, %ReferenceServer1%, %TempFile%

  ; Check if there was an error.
  if ErrorLevel {

    ; Because of the error try the second reference server.
    UrlDownloadToFile, %ReferenceServer2%, %TempFile%

    ; Check if there was an error.
    if ErrorLevel {

      ; Set the GUI color.
      Gui, Color, CC0033

      ; Check if the error flag is not enabled.
      if (ErrorFlag = 0) {

        ; Check if the audio is enabled.
        if (AudioOn = 1) {

          ; Give an alarm signal.
          SoundPlay, %A_ScriptDir%\sound\alarm.wav

        } ; Check if the audio is enabled.

        ; Check if the balloon messages should be displayed.
        if (BalloonOn = 1) {

          ; Display an error balloon message.
          TrayTip, %ApplName% %ApplVersion%, No Internet connection available !, 10, 3

        } ; Check if the balloon messages should be displayed.

        ; Enable the error flag.
        ErrorFlag = 1

      } ; Check if the error flag is not enabled.

    } ; Check if there was an error.

    ; The connection is ok.
    else {

      ; Set the GUI color.
      Gui, Color, 1DB400

      ; Check if the error flag is enabled.
      if (ErrorFlag = 1) {

        ; Check if the audio is enabled.
        if (AudioOn = 1) {

          ; Give an ok signal.
          SoundPlay, %A_ScriptDir%\sound\ok.wav

        } ; Check if the audio is enabled.

        ; Check if the balloon messages should be displayed.
        if (BalloonOn = 1) {

          ; Display an error balloon message.
          TrayTip, %ApplName% %ApplVersion%, The Internet connection is available again !`nThe first reference server was not available !, 10, 1

        } ; Check if the balloon messages should be displayed.

        ; Disable the error flag.
        ErrorFlag = 0

      } ; Check if the error flag is enabled.

    } ; The connection is ok.

  } ; Check if there was an error.

  ; The connection is ok.
  else {

    ; Set the GUI color.
    Gui, Color, 1DB400

    ; Check if the error flag is enabled.
    if (ErrorFlag = 1) {

      ; Check if the audio is enabled.
      if (AudioOn = 1) {

        ; Give an ok signal.
        SoundPlay, %A_ScriptDir%\sound\ok.wav

      } ; Check if the audio is enabled.

      ; Check if the balloon messages should be displayed.
      if (BalloonOn = 1) {

        ; Display an information balloon message.
        TrayTip, %ApplName% %ApplVersion%, The Internet connection is available again !, 10, 1

      } ; Check if the balloon messages should be displayed.

      ; Disable the error flag.
      ErrorFlag = 0

    } ; Check if the error flag is enabled.

  } ; The connection is ok.

	; Sleep a while.
	Sleep, %SleepTime%

} ; Loop to check the internet connection

; End of the main script.
return


; ======================================================================
; E N A B L E / D I S A B L E   T H E   A U D I O   A L A R M S
; ======================================================================

; Specify the hotkey to enable/diable the audio alarms - CTRL + ALT + A
^!a::

  ; Check if the audio is enabled.
  if (AudioOn = 1) {

    ; Disable the audio.
    AudioOn = 0

    ; Check if the balloon messages should be displayed.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Audio is DISABLED, 5, 1

    } ; Check if the balloon messages should be displayed.

  } ; Check if the audio is enabled.

  ; The audio is not enabled.
  else {

    ; Enable the audio.
    AudioOn = 1

    ; Check if the balloon messages should be displayed.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Audio is ENABLED, 5, 1

    } ; Check if the balloon messages should be displayed.

  } ; The audio is not enabled.

return


; ======================================================================
; E N A B L E / D I S A B L E   T H E   B A L L O O N   M E S S A G E S
; ======================================================================

; Specify the hotkey to enable/disable the balloon messages - CTRL + ALT + B
^!b::

  ; Check if the balloon messages are enabled.
  if (BalloonOn = 1) {

    ; Disable the balloon messages.
    BalloonOn = 0

    ; Display an information balloon message.
    TrayTip, %ApplName% %ApplVersion%, Balloon messages are DISABLED, 5, 1

  } ; Check if the balloon messages are enabled.

  ; The balloon messages are not enabled.
  else {

    ; Enable the balloon messages.
    BalloonOn = 1

    ; Display an information balloon message.
    TrayTip, %ApplName% %ApplVersion%, Balloon messages are ENABLED, 5, 1

  } ; The balloon messages are not enabled.

return


; ======================================================================
; S H O W / H I D E   T H E   G U I / W I N D O W   C A P T I O N
; ======================================================================

; Specify the hotkey to hide the GUI/window caption - CTRL + ALT + C
^!c::

  ; Check if the window caption is displayed.
  if (CaptionFlag = 0) {

    ; Enable the caption flag.
    CaptionFlag = 1

    ; Display the window caption.
    Gui, +Caption

    ; Check if the balloon messages are enabled.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Show window caption..., 10, 1

    } ; Check if the balloon messages are enabled.

  } ; Check if the window caption is displayed.

  ; The window caption is not displayed.
  else {

    ; Disable the caption flag.
    CaptionFlag = 0

    ; Hide the window caption.
    Gui, -Caption

    ; Check if the balloon messages are enabled.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Hide window caption..., 10, 1

    } ; Check if the balloon messages are enabled.

  } ; The window caption is not displayed.

return


; ======================================================================
; D E C R E A S E   T H E   S L E E P   T I M E
; ======================================================================

; Specify the hotkey to decrease the sleep time 10 seconds - CTRL + ALT + DOWN ARROW
^!down::

  ; Decrease the sleep time.
  SleepTime := SleepTime - 10000

  ; Check if the sleep time is lower than the minimum.
  if (SleepTime < (MinSleepTime * 1000)) {

    ; Set the sleep time to the minimum.
    SleepTime := MinSleepTime * 1000

  } ; Check if the sleep time is lower than the minimum.

  ; Specify the display time.
  DisplayTime := Round(SleepTime / 1000)

  ; Check if the balloon messages are enabled.
  if (BalloonOn = 1) {

    ; Display an information balloon message.
    TrayTip, %ApplName% %ApplVersion%, Sleep time decreased to %DisplayTime% seconds !, 5, 1

  } ; Check if the balloon messages are enabled.

return


; ======================================================================
; S H O W / H I D E   T H E   G U I / W I N D O W
; ======================================================================

; Specify the hotkey to show/hide the GUI/window - CTRL + ALT + G
^!g::

  ; Check if the status window is displayed.
  IfWinExist, %ApplName%
  {

    ; Hide the status window.
    WinHide,  %ApplName%

    ; Disable the GUI flag.
    GUIFlag = 0

    ; Check if the balloon messages are enabled.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Hide status window..., 10, 1

    } ; Check if the balloon messages are enabled.

  } ; Check if the status window is displayed.

  ; The status window is not displayed.
  else {

    ; Display the status window.
    WinShow, %ApplName%

    ; Enable the GUI flag.
    GUIFlag = 1

    ; Check if the balloon messages are enabled.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Show status window..., 10, 1

    } ; Check if the balloon messages are enabled.

  } ; The status window is not displayed.

return


; ======================================================================
; S H O W   T H E   H O T K E Y / S H O R T C U T   W I N D O W
; ======================================================================

; Specify the hotkey to show the hotkey/shortcut window - CTRL + ALT + H
^!h::

  ; Specify the information text.
  InfoText = The following hotkeys/shortcuts are available:`n`n

  ; Add all shortcut combinations.
  InfoText = %InfoText%CTRL + ALT + A    -->  Toggle audio (on/off)`n
  InfoText = %InfoText%CTRL + ALT + B    -->  Toggle balloon messages (on/off)`n
  InfoText = %InfoText%CTRL + ALT + C    -->  Toggle windows caption (show/hide)`n
  InfoText = %InfoText%CTRL + ALT + G    -->  Toggle GUI (show/hide)`n
  InfoText = %InfoText%CTRL + ALT + H    -->  Show this hotkey/shortcut window`n
  InfoText = %InfoText%CTRL + ALT + O   -->  Open the configuration file`n
  InfoText = %InfoText%CTRL + ALT + P    -->  Save actual window position`n
  InfoText = %InfoText%CTRL + ALT + R    -->  Reload the application`n
  InfoText = %InfoText%CTRL + ALT + S    -->  Display configuration settings`n
  InfoText = %InfoText%CTRL + ALT + T    -->  Toggle window on top (on/off)`n
  InfoText = %InfoText%CTRL + ALT + V    -->  Display the application version`n
  InfoText = %InfoText%CTRL + ALT + W   -->  Display the PB-Soft website`n
  InfoText = %InfoText%CTRL + ALT + X    -->  Exit the application`n`n
  InfoText = %InfoText%CTRL + DOWN-ARROW -->  Decrease sleep time (-10s)`n
  InfoText = %InfoText%CTRL + UP-ARROW       -->  Increase sleep time (+10s)`n`n

  ; Display an information message.
  MsgBox, 4160, %ApplName% %ApplVersion%, %InfoText%

return


; ======================================================================
; O P E N   T H E   C O N F I G U R A T I O N   F I L E
; ======================================================================

; Specify the hotkey to show the configuration file - CTRL + ALT + O
^!o::

  ; Check if the configuration file exist.
  IfExist, %ConfigurationFile%
  {

    ; Open the configuration file.
    Run, %ConfigurationFile%

  } ; Check if the configuration file exist.

  ; There is no configuration file available.
  else
  {

    ; Display an error message.
    MsgBox, 4112, %ApplName% %ApplVersion%, There is no configuration file available !

  } ; There is no configuration file available.

return


; ======================================================================
; S A V E   T H E   S T A T U S   W I N D O W   P O S I T I O N
; ======================================================================

; Specify the hotkey to save the status window position - CTRL + ALT + P
^!p::

  ; Get the position of the status window.
  WinGetPos, GUIPosX, GUIPosY, , , A

  ; Save the actual window position.
  IniWrite, %A_Space%%GUIPosX%, %ConfigurationFile%, Configuration, WindowPositionX
  IniWrite, %A_Space%%GUIPosY%, %ConfigurationFile%, Configuration, WindowPositionY

  ; Check if the balloon messages should be displayed.
  if (BalloonOn = 1) {

    ; Display an information balloon message.
    TrayTip, %ApplName% %ApplVersion%, Window position successfully saved!, 5, 1

  } ; Check if the balloon messages should be displayed.

return


; ======================================================================
; R E S T A R T / R E L O A D   T H E   A P P L I C A T I O N
; ======================================================================

; Specify the hotkey to reload the application - CTRL + ALT + R
^!r::

  ; Restart the application.
  Reload

Return


; ======================================================================
; D I S P L A Y   C O N F I G U R A T I O N   S E T T I N G S
; ======================================================================

; Specify the hotkey to display the configuration settings - CTRL + ALT + S
^!s::

  ; Specify the display time.
  DisplayTime := Round(SleepTime / 1000)

  ; Display an information balloon message.
  TrayTip, %ApplName% %ApplVersion%, GUI: %GUIFlag%`nTop: %OnTop%`nCaption: %CaptionFlag%`nAudio: %AudioOn%`nBalloon: %BalloonOn%`nSleep: %DisplayTime%, 10, 1

return


; ======================================================================
; E N A B L E / D I S A B L E   W I N D O W   O N   T O P
; ======================================================================

; Specify the hotkey to enable/diable the window on top setting - CTRL + ALT + T
^!t::

  ; Check if the window on top setting is enabled.
  if (OnTop = 1) {

    ; Disable the window on top setting.
    OnTop = 0

    ; Check if the balloon messages should be displayed.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Window on top setting is DISABLED, 5, 1

    } ; Check if the balloon messages should be displayed.

  } ; Check if the window on top setting is enabled.

  ; The window on top setting is not enabled.
  else {

    ; Enable the window on top setting.
    OnTop = 1

    ; Check if the balloon messages should be displayed.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Window on top setting is ENABLED, 5, 1

    } ; Check if the balloon messages should be displayed.

  } ; The window on top setting is not enabled.

  ; Save the actual window on top setting.
  IniWrite, %A_Space%%OnTop%, %ConfigurationFile%, Configuration, OnTop

  ; Restart the application.
  Reload

return


; ======================================================================
; I N C R E A S E   T H E   S L E E P   T I M E
; ======================================================================

; Specify the hotkey to increase the sleep time 10 seconds - CTRL + ALT + UP ARROW
^!up::

  ; Increase the sleep time.
  SleepTime := SleepTime + 10000

  ; Specify the display time.
  DisplayTime := Round(SleepTime / 1000)

  ; Check if the balloon messages are enabled.
  if (BalloonOn = 1) {

    ; Display an information balloon message.
    TrayTip, %ApplName% %ApplVersion%, Sleep time increased to %DisplayTime% seconds !, 5, 1

  } ; Check if the balloon messages are enabled.

return


; ======================================================================
; D I S P L A Y   T H E   A P P L I C A T I O N   V E R S I O N
; ======================================================================

; Specify the hotkey to show the application version - CTRL + ALT + V
^!v::

  ; Display the application version.
  MsgBox, 4160, %ApplName% %ApplVersion%, Application Name: %ApplName%`n`nApplication Version: %ApplVersion%`n`nInternal Version: %ApplIntVersion%`n`nApplication Build Date: %ApplBuildDate%`n`nWebsite: %ApplWebsite%`n`n%Copyright%

return


; ======================================================================
; O P E N   T H E   C O M P A N Y   W E B S I T E
; ======================================================================

; Specify the hotkey to show the company website - CTRL + ALT + W
^!w::

  ; Check if the Internet connection is OK.
  if (ErrorFlag = 0) {

    ; Check if the balloon messages are enabled.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Display the PB-Soft website..., 10, 1

    } ; Check if the balloon messages are enabled.

    ; Open the PB-Soft website.
    Run, %ApplWebsite%, %A_ScriptDir%, UseErrorLevel

    ; Check if the website could not be opened.
    if ErrorLevel = ERROR
    {

      ; Display an error message.
      MsgBox, 4112, %ApplName% %ApplVersion%, There is no default application specified for opening websites !

    } ; Check if the website could not be opened.

  } ; Check if the Internet connection is OK.

  ; There is no Internet connection available.
  else
  {

    ; Display an error message.
    MsgBox, 4112, %ApplName% %ApplVersion%, There is no Internet connection available !

  } ; There is no Internet connection available.

return


; ======================================================================
; E X I T   T H E   C H E C K N E T   A P P L I C A T I O N
; ======================================================================

; Specify the hotkey to exit the application - CTRL + ALT + X
^!x::

  ; Display an exit question.
  MsgBox, 4132, %ApplName% %ApplVersion%, Do you really want to exit ?

  ; Check if the yes button was pressed.
  IfMsgBox Yes
  {

    ; Delete the old and therefore unnecessary files.
    FileDelete, %TempFile%

    ; Check if the balloon messages should be displayed.
    if (BalloonOn = 1) {

      ; Display an information balloon message.
      TrayTip, %ApplName% %ApplVersion%, Exit the %ApplName% application..., 5, 1

    } ; Check if the balloon messages should be displayed.

    ; Sleep some time.
    Sleep, 2000

    ; Exit the application.
    ExitApp

  } ; Check if the yes button was pressed.

return
